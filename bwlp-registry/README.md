# bwlp-registry

implementation of the backend system for bwlp to manage container images. 

## registry-server

Server Application wich runs on the bwlp sat server. This component manages container specific requests against the server.
(e.g. pull of images, storage of images, creation of images, ...). This Server also provides an endpoint to retrieve all defined images.

### Prerequisite

This server needs the newest version of dozmod-server(dmsd) to work. Follow the [README](https://gitlab.com/risenman/bwlehrpool-vmchooser-docker) to set it up.

### Environment

**Install Conda Python on Sat**

```shell
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# confirm license
# confirm conda init with 'yes'

conda config --set auto_activate_base false
```

**Set up container_registry**

```shell
cd /opt
git clone https://gitlab.com/risenman/bwlp-prototyp.git
cd bwlp-prototyp/bwlp-registry

conda create -p /opt/bwlp-prototyp/bwlp-registry/condaenv python=3.8.5
conda activate /opt/bwlp-prototyp/bwlp-registry/condaenv

pip install -r requirements
python -m src.registry_server
```
To run the registry_server permanently run the server in a tmux session! 
If everything (dmsd, bwlp_registry) is properly set up a curl against `<HOSTNAME>:9081/v2/v2/bwlp/images/` should return a list of registerd container images (only batch and data type)


### Image Pull

Also pull against this server are possible. This makes a anonymous pull against docker hub

```shell
docker pull localhost:9081/ralphhso/python_minesweeper:latest
```

### TODO 
- file handling of stored container images



## !The following needs an update!

## environment

- Python 3.8
- venv


```bash
# make own python environment in project dir
git clone https://gitlab.com/risenman/bwlp-prototyp

cd ./bwlp-prototyp/bwlp-registry
python -m venv bwlp_registry_venv

source ./bwlp_registry_venv/bin/activate
pip install -r requirements

# ipython for dev
pip install ipython 
```

## start proxy instance (bwlp-clients)

```bash
# start proxy server with port=20000
python src/registry_proxy.py 20000

# docker pull to proxy server, redirect to server instance
docker pull localhost:20000/ralphhso/python_minesweeper:latest
```
