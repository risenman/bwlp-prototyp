import os
import shutil
import unittest
import datetime
from unittest.mock import MagicMock

from src import registry_proxy


registry_proxy.logger.removeHandler(registry_proxy.steamHandler)

test_image_name = "library/python"
test_image_tag = "3.7"

test_blob_digest = "sha256:56b703a5a3712ccf9712ec814fe143c9727291bfefd0afb95ab053195c233d15"


def createProcessMock():
    # return string which we are looking for in dnbd3_mount_file
    def readline_mock(*args, **kwargs): return "ImagePathName"
    process_mock = MagicMock()
    process_mock.stdout = MagicMock()
    process_mock.stdout.readline = readline_mock
    mock_execute = MagicMock(return_value=(process_mock))
    return mock_execute


class test_RegistryTestCase(unittest.TestCase):

    def setUp(self):
        pass

    def test_CreateDndb3Location(self):
        parent_directory = registry_proxy.dnbd3_parent_dir
        registry_proxy.create_mount_dir(parent_directory)
        self.assertTrue(os.path.isdir(parent_directory),
                        "Directory: {} does not exist".format(parent_directory))

        shutil.rmtree("/tmp/docker")

    def test_dnbd3ManifestMountParameter(self):

        manifest_location = os.path.join(
            registry_proxy.dnbd3_parent_dir, "manifest")

        self.assertFalse(os.path.isfile(manifest_location),
                         "File: {} should not exist".format(manifest_location))

        mock_execute = createProcessMock()

        # Do Testing
        registry_proxy.dnbd3_mount_file(dnbd3_opt_remote_path=registry_proxy.dnbd3_opt_manifest_path.format(test_image_name+"/"+test_image_tag),
                                        dnbd3_opt_local_mount=manifest_location,
                                        execute=mock_execute)

        self.assertTrue(os.path.isdir(manifest_location),
                        "Directory: {} does not exist".format(manifest_location))

        test_cmd = registry_proxy.dnbd3_cmd +\
            registry_proxy.dnbd3_opt + \
            registry_proxy.dnbd3_opt_default_host + \
            registry_proxy.dnbd3_opt_manifest_path \
            .format(test_image_name+"/"+test_image_tag) + " " + \
            manifest_location

        mock_execute.assert_called_with(test_cmd.split())

    def test_dnbd3BlobMountParameter(self):
        test_blob_mount = os.path.join(
            registry_proxy.dnbd3_parent_dir, test_blob_digest)

        mock_execute = createProcessMock()

        registry_proxy.dnbd3_mount_file(dnbd3_opt_remote_path=registry_proxy.dnbd3_opt_blob_path.format(test_blob_digest),
                                        dnbd3_opt_local_mount=test_blob_mount,
                                        execute=mock_execute)

        self.assertTrue(os.path.isdir(test_blob_mount),
                        "Directory: {} does not exist".format(test_blob_mount))

        test_cmd = registry_proxy.dnbd3_cmd +\
            registry_proxy.dnbd3_opt + \
            registry_proxy.dnbd3_opt_default_host + \
            registry_proxy.dnbd3_opt_blob_path.format(test_blob_digest) + " " + \
            test_blob_mount

        mock_execute.assert_called_with(test_cmd.split())

    def test_ReadInChunks(self):
        test_data = [b"1\n", b"2\n", b"3\n"]
        test_file = os.path.join(os.path.dirname(__file__), "test_file")
        with open(test_file, "rb") as file_obj:
            for i, data in enumerate(registry_proxy.read_in_chunks(file_obj, chunk_size=2)):
                self.assertEqual(type(b""), type(data))
                self.assertEqual(test_data[i], data)

    def test_PrepareHttpHeaderBlob(self):
        content_lenght = 42
        response = registry_proxy.prepare_http_header(
            "blob", content_lenght, test_blob_digest)
        r = registry_proxy.base_response
        r["headers"] = {**r["headers"], **registry_proxy.blob_header}
        r["headers"]["docker-content-digest"] = test_blob_digest
        r["headers"]["etag"] = test_blob_digest
        r["headers"]["content_length"] = content_lenght
        r["headers"]["date"] = datetime.datetime.now().strftime(
            "%a, %d %b %Y %X %Z")
        self.assertDictEqual(response, r)

    def test_PrepareHttpHeaderManifest(self):
        content_lenght = 42
        response = registry_proxy.prepare_http_header(
            "manifest", content_lenght)
        r = registry_proxy.base_response
        r["headers"] = {**r["headers"], **registry_proxy.manifest_headers}
        r["headers"]["content_length"] = content_lenght
        r["headers"]["date"] = datetime.datetime.now().strftime(
            "%a, %d %b %Y %X %Z")
        self.assertDictEqual(response, r)

    def test_SubprocessExecutionOk(self):
        p = registry_proxy.exec_subprcess(cmd=["ls"])
        self.assertEqual(None, p.returncode)

    # def test_SubprocessExecutionFail(self):
    #     p = registry_proxy.exec_subprcess(cmd=["exit 1"])
    #     self.assertEqual(1, p.returncode)

    # def test_SubprocessCheckStdoutSimple(self):
    #     test_string = "Hello World\n"
    #     p = registry_proxy.exec_subprcess(
    #         cmd=["echo {}".format(test_string)])
    #     #self.assertEqual(None, p.returncode)
    #     #self.assertEqual(test_string, p.stdout.readline())

    def test_SubprocessCheckStdoutDnbd3(self):
        first_line = "[24.03. 11:40:13] Info: Got 1 servers from init call\n"
        test_file = os.path.join(
            os.path.dirname(__file__), "test_dnbd3_output")

        p = registry_proxy.exec_subprcess(
            cmd=["/bin/bash", "-c", "head -n1 {}".format(test_file)])

        self.assertEqual(None, p.returncode)
        self.assertEqual(first_line, p.stdout.readline())

    def test_SubprocessDnbd3WaitForOutput(self):
        test_line = "ImagePathName: /img\n"
        test_file = os.path.join(
            os.path.dirname(__file__), "test_dnbd3_output")

        p = registry_proxy.exec_subprcess(
            cmd=["/bin/bash", "-c", "while read line; do echo $line; sleep .5; done < {}".format(test_file)])

        self.assertEqual(None, p.returncode)
        p.stdout.readline()
        p.stdout.readline()
        p.stdout.readline()
        p.stdout.readline()
        self.assertEqual(test_line, p.stdout.readline())


if __name__ == '__main__':
    unittest.main()
