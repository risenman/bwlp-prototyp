import unittest
from pathlib import Path
from src.digest_store import DigestStore

TEST_BASE_DIR = Path(__file__).resolve().parent


class test_CacheDict(unittest.TestCase):

    def setUp(self) -> None:
        self.testee = DigestStore()

    def test_AddAndReceiveEntry(self):
        test_digest = "sha256:628a915caf0da66036c2a712d2161ac1f5d9bbb76ec2859bb4149b67f66d686c"

        expected_image_name = "library/python"
        expected_image_tag = 3.2

        self.testee.add_entry(test_digest,
                              expected_image_name,
                              expected_image_tag)

        self.assertIn(test_digest, self.testee)
        (name, tag) = self.testee.get_entry(test_digest)
        self.assertEqual(name, expected_image_name, "name not equal")
        self.assertEqual(tag, expected_image_tag, "tag not equal")

    def test_AddMultipleEntries(self):
        test_digest = ["sha256:628a915caf0da66036c2a712d2161ac1f5d9bbb76ec2859bb4149b67f66d686c",
                       "sha256:18f9b4e2e1bcd5abe381a557c44cba379884c88f6049564f58fd8c10ab5733df",
                       "sha256:aa31437d461e2e27f19a40cec530dc1098baf6a3593e7b178ab0e50ef9f50fc5"]

        expected_image_name = ["library/python",
                               "library/python",
                               "library/openjdk"]
        expected_image_tag = ["3.2", "3.3", "11"]

        for i, digest in enumerate(test_digest):
            print(i, digest)
            self.testee.add_entry(digest,
                                  expected_image_name[i],
                                  expected_image_tag[i])

        for i, digest in enumerate(test_digest):
            self.assertIn(digest, self.testee)
            (name, tag) = self.testee.get_entry(digest)
            self.assertEqual(name, expected_image_name[i], "name not equal")
            self.assertEqual(tag, expected_image_tag[i], "tag not equal")

    def test_AddEntriesWithEqualDigest(self):
        test_digest = "sha256:628a915caf0da66036c2a712d2161ac1f5d9bbb76ec2859bb4149b67f66d686c"

        expected_image_name = ["library/python",
                               "library/ubuntu",
                               "library/openjdk"]
        expected_image_tag = ["3.2", "3.3", "11"]

        for i in range(3):
            self.testee.add_entry(
                test_digest, expected_image_name[i], expected_image_tag[i])

        self.assertIn(test_digest, self.testee)
        (name, tag) = self.testee.get_entry(test_digest)
        self.assertIn(name, expected_image_name, "name not equal")
        self.assertIn(tag, expected_image_tag, "tag not equal")
        # name and tag should come from the same tuple
        self.assertEqual(expected_image_name.index(name),
                         expected_image_tag.index(tag), "index not equal")

    def test_InsertManifest(self):
        manifest_file = Path.joinpath(TEST_BASE_DIR,
                                      "ralphhso_alpine_manifest")

        expected_image_name = "ralphhso/alpine"
        expected_image_tag = "latest"

        expected_digest = ["sha256:6dbb9cc54074106d46d4ccb330f2a40a682d49dda5f4844962b7dce9fe44aaec",
                           "sha256:540db60ca9383eac9e418f78490994d0af424aab7bf6d0e47ac8ed4e2e9bcbba"]

        with open(manifest_file, "rb") as f:
            binary_manifest = f.read()

        self.testee.insert_manifest(
            binary_manifest, expected_image_name, expected_image_tag)

        self.assertIn(expected_digest[0], self.testee)
        self.assertIn(expected_digest[1], self.testee)
        for d in expected_digest:
            (name, tag) = self.testee.get_entry(d)
            self.assertEqual(name, expected_image_name)
            self.assertEqual(tag, expected_image_tag)


if __name__ == '__main__':
    unittest.main()
