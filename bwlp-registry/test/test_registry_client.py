import unittest
import pathlib

from src.registry_client import DockerHubClient


class test_DockerHubClient(unittest.TestCase):

    def setUp(self) -> None:
        self.testee = DockerHubClient()

    def test_RegistryEntry(self):

        r = self.testee.request_api_endpoint()
        self.assertEqual(r.status_code, 200,
                         "/V2/ request was not successfull")

    def test_GetAuthUrl(self):
        realm = 'Bearer realm="https://auth.docker.io/token",service="registry.docker.io"'
        expectedUrl = "https://auth.docker.io/token?service=registry.docker.io"
        url = self.testee._get_auth_url(realm=realm)

        self.assertEqual(url, expectedUrl,
                         "Authentication Url is not as expected")

    def test_GetAuthUrlWithScope(self):
        realm = 'Bearer realm="https://auth.docker.io/token",service="registry.docker.io",scope="repository:library/python:pull"'
        expectedUrl = "https://auth.docker.io/token?service=registry.docker.io&scope=repository:library/python:pull"
        url = self.testee._get_auth_url(realm=realm)

        self.assertEqual(url, expectedUrl,
                         "Authentication Url is not as expected")

    def test_GetAuthToken(self):
        realm = 'Bearer realm="https://auth.docker.io/token",service="registry.docker.io"'
        token = self.testee._get_auth_token(realm=realm)

        self.assertIsNotNone(token, "Token is None")
        self.assertNotEqual(token, "", "Empty Token")
        # don't know if there is token schema
        self.assertTrue(len(token) > 10, "Token is very short, pleas check!")

    def test_GetImageManifest(self):
        image_name = "library/python"
        image_reference = "3.7"

        r = self.testee.request_manifest(image_name, image_reference)
        self.assertEqual(r.status_code, 200,
                         "manifest request was not successfull")

    def test_RequestImageLayer(self):
        # test Request, with an well known target, use helper method to write to disk.
        image_name = "ralphhso/alpine"
        digest = "sha256:540db60ca9383eac9e418f78490994d0af424aab7bf6d0e47ac8ed4e2e9bcbba"

        test_dir = pathlib.Path(__file__).resolve().parent
        test_layer = test_dir.joinpath(digest)
        test_layer_size = 2811969

        self.testee._write_to_disk(image_name, digest, test_layer)
        self.assertEqual(test_layer.stat().st_size,
                         test_layer_size, "layer size is not as expected")
        test_layer.unlink(missing_ok=True)

    def test_HeadRequestManifest(self):
        image_name = "ralphhso/alpine"
        image_reference = "latest"

        # expected values for ralphhso/alpine
        expected_content_length = 528
        expected_docker_content_digest = "sha256:def822f9851ca422481ec6fee59a9966f12b351c62ccb9aca841526ffaa9f748"
        r = self.testee.head_manifest(image_name, image_reference)
        self.assertEqual(r.status_code, 200,
                         "manifest request was not successfull")
        self.assertIn("content-length", r.headers,
                      "content-length not in response header")
        self.assertEqual(
            int(r.headers["content-length"]), expected_content_length, "content-length not equal")
        self.assertIn("docker-content-digest",
                      r.headers,
                      "docker-content-digest not in response header")
        self.assertEqual(r.headers["docker-content-digest"],
                         expected_docker_content_digest,
                         "content-length not equal")

    def test_RequestRateLimit(self):

        r = self.testee.request_pullratelimit()
        self.assertEqual(r.status_code, 200,
                         "ratelimit request not successful")
        self.assertIn("ratelimit-limit", r.headers,
                      "ratelimit-limit non in header")
        self.assertIn("ratelimit-remaining", r.headers,
                      "ratelimit-remaining not in header")


ralphhso_alpine_manifest = {
    "schemaVersion": 2,
    "mediaType": "application/vnd.docker.distribution.manifest.v2+json",
    "config": {
        "mediaType": "application/vnd.docker.container.image.v1+json",
        "size": 1472,
        "digest": "sha256:6dbb9cc54074106d46d4ccb330f2a40a682d49dda5f4844962b7dce9fe44aaec"
    },
    "layers": [
        {
            "mediaType": "application/vnd.docker.image.rootfs.diff.tar.gzip",
            "size": 2811969,
            "digest": "sha256:540db60ca9383eac9e418f78490994d0af424aab7bf6d0e47ac8ed4e2e9bcbba"
        }
    ]
}

if __name__ == '__main__':
    unittest.main()
