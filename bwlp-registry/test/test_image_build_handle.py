import unittest
from pathlib import Path
from src.image_build_handle import BwlpImageBuildHandle

TEST_BASE_DIR = Path(__file__).resolve().parent


class test_BwlpImageBuildHandle(unittest.TestCase):

    def setUp(self) -> None:
        self.testee = BwlpImageBuildHandle()

    def test_IsNotConnected(self):
        self.assertEqual(self.testee._is_connected(),
                         False, "Client should not be connected")

    def test_ConnectClient(self):
        self.testee._open_connection()

        self.assertEqual(self.testee._is_connected(),
                         True, "Client should be connected")

        self.testee._close_connection()

        self.assertEqual(self.testee._is_connected(),
                         False, "Client should not be connected")

    # TODO How to Test SSH Session with executed Bash Scripts?
    # currently manually


if __name__ == '__main__':
    unittest.main()
