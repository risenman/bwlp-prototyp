from time import sleep
import unittest
import string

from src.cache import CacheDict


class test_CacheDict(unittest.TestCase):

    def setUp(self) -> None:
        self.testee = CacheDict(ttl=2)

    def test_AddEntry(self):
        key, val = 2, 'A'

        self.testee.add_entry(key, val)
        retval = self.testee.get_entry(2)
        self.assertEqual(retval, val, "not expected Value")

    def test_AddMultipleEntries(self):
        keys,  values = [1, 2, 3], list(string.ascii_uppercase[:3])
        for k, v in zip(keys, values):
            self.testee.add_entry(k, v)

        for k, v in zip(keys, values):
            retval = self.testee.get_entry(k)
            self.assertIsNotNone(retval, "retval ist none")
            self.assertEqual(retval, v, "not expected Value")

    def test_CheckTtlOfEntry(self):
        key, val = 2, 'A'
        self.testee.add_entry(key, val)
        sleep(2)
        retval = self.testee.get_entry(key)
        self.assertIsNone(retval, "retval was not none")


if __name__ == '__main__':
    unittest.main()
