import unittest

from src.registry_server import BwlpRegistryHandler

localhost = "http://127.0.0.1:8080"


class test_RegistryServer(unittest.TestCase):

    def test_SplitString(self):
        split_str = "/manifests/"
        test_strings = ["/v2/library/python", "3.7"]

        t = split_str.join(test_strings).split(split_str)
        self.assertEqual(
            2, len(t), "SimpleTest: String.split works not as accepted")
        self.assertEqual(test_strings, t)

    def test_ExtractInfoFromManifestPath(self):
        expected_image_name = "library/python"
        expected_reference = "3.7"
        context = "manifests"

        test_path = f"/v2/{expected_image_name}/{context}/{expected_reference}"

        (image_name, reference) = BwlpRegistryHandler._get_context_info(test_path,
                                                                        context)

        self.assertEqual(image_name, expected_image_name,
                         "image_name is not correct")
        self.assertEqual(reference, expected_reference,
                         "reference is not correct")

    def test_ExtractInfoFromBlobPath(self):
        expected_image_name = "library/python"
        expected_digest = "sha256:56b703a5a3712ccf9712ec814fe143c9727291bfefd0afb95ab053195c233d15"
        context = "blobs"
        test_path = f"/v2/{expected_image_name}/{context}/{expected_digest}"

        (image_name, digest) = BwlpRegistryHandler._get_context_info(test_path,
                                                                     context)

        self.assertEqual(image_name, expected_image_name,
                         "image_name is not correct")
        self.assertEqual(digest, expected_digest, "reference is not correct")


if __name__ == '__main__':
    unittest.main()
