import requests
import json
import os
import shutil
from pathlib import Path
import argparse


server_name = "http://bwgpul2.cit.hs-offenburg.de"
server_port = 5000


class DockerHubClient():
    def __init__(self) -> None:
        self.host = "https://index.docker.io"
        self.headers = {
            "Accept": "application/vnd.docker.distribution.manifest.v2+json"}

    def _get_auth_url(self, realm) -> str:
        _realm = realm.replace("Bearer realm", "url")
        _realm = dict(x.split("=") for x in _realm.split(","))
        for (k, v) in _realm.items():
            _realm[k] = str(v).replace('"', '')

        auth_url = f"{_realm['url']}?service={_realm['service']}"
        if "scope" in _realm:
            auth_url = f"{auth_url}&scope={_realm['scope']}"
        return auth_url

    def _get_auth_token(self, realm: str) -> str:
        url = self._get_auth_url(realm)
        r = requests.get(url=url)

        assert r.status_code == 200
        assert 'token' in r.json()

        return r.json()['token']

    def _request_auth_token(self, url) -> str:
        r = requests.get(url=url)

        assert r.status_code == 200
        assert 'token' in r.json()

        return r.json()['token']

    def _prepare_auth_request(self, response):
        token = self._get_auth_token(response.headers["Www-Authenticate"])
        self.headers["Authorization"] = f"Bearer {token}"

    def _write_to_disk(self, name, digest, filename):
        # helper method: starts a blob request with name and digest
        # and stores the downloaded content in filename
        r = self.request_blob(name, digest)
        with open(filename, mode="wb") as outfile:
            for chunk in r.iter_content(chunk_size=128):
                outfile.write(chunk)

    def _do_request(self, _request: str, stream=None):
        r = requests.get(url=_request, stream=stream)
        if r.status_code == 200:
            return r
        self._prepare_auth_request(r)
        r = requests.get(url=_request, headers=self.headers, stream=stream)
        return r

    def _do_head(self, _request: str):
        r = requests.head(url=_request)
        if r.status_code == 200:
            return r
        self._prepare_auth_request(r)
        r = requests.get(url=_request, headers=self.headers)
        return r

    def request_api_endpoint(self) -> requests.Response:
        _request = f"{self.host}/v2/"
        return self._do_request(_request)

    def request_manifest(self, name, reference) -> requests.Response:
        _request = f"{self.host}/v2/{name}/manifests/{reference}"
        return self._do_request(_request)

    def request_blob(self, name, digest) -> requests.Response:
        _request = f"{self.host}/v2/{name}/blobs/{digest}"
        return self._do_request(_request, stream=True)

    def head_manifest(self, name, reference) -> requests.Response:
        _request = f"{self.host}/v2/{name}/manifests/{reference}"
        return self._do_head(_request)

    def request_pullratelimit(self):
        _auth_url = "https://auth.docker.io/token?service=registry.docker.io&scope=repository:ratelimitpreview/test:pull"
        token = self._request_auth_token(_auth_url)
        self.headers["Authorization"] = f"Bearer {token}"
        _url = "https://registry-1.docker.io/v2/ratelimitpreview/test/manifests/latest"
        return requests.head(url=_url, headers=self.headers)


class BwlpTestClient():

    def __init__(self):
        self.dh_client = DockerHubClient()

    def download_image(self, target_dir, name, reference):
        r = self.dh_client.request_manifest(name, reference)
        assert r.status_code == 200, "Request not successfull"

        target_dir = Path(target_dir).resolve()
        if not target_dir.exists():
            target_dir.mkdir()
        else:
            shutil.rmtree(str(target_dir))
            target_dir.mkdir()

        manifest_data = r.json()
        manifest_file = target_dir.joinpath("manifest")
        layer_dir = target_dir.joinpath("layer")
        layer_dir.mkdir(exist_ok=True)
        with open(manifest_file, mode="w") as f:
            f.write(json.dumps(manifest_data))

        digest = [manifest_data["config"]["digest"]] + \
            [l["digest"] for l in manifest_data["layers"]]
        for d in digest:
            blob = layer_dir.joinpath(str(d))
            with open(blob.as_posix(), "bw") as f:
                r = self.dh_client.request_blob(name, d)
                for chunk in r.iter_content(chunk_size=1024):
                    f.write(chunk)


def main(args):
    print("Making Requests")
    s = requests.Session()
    server = "{}:{}".format(server_name, server_port)
    print("Server: ", server)

    # actions = {'check': check_registry,
    #            'list_images': get_images,
    #            'list_tags': get_repo_tags,
    #            'get_manifest': get_image_manifest,
    #            'get_blobs': get_image_blobs}

    # f = actions.get(args.action, lambda: 'invalid')

    container_registry = "{}/v2".format(server)
    # f(s, container_registry)

    # return

    repo_name = "library/python"
    repo_tag = "3.7"
    options = "layers"
    options = None

    path = os.path.dirname(__file__)

    # get_repo_tags(s, container_registry, repo_name=repo_name)

    # get_image_manifest(s, container_registry=container_registry, repo_name=repo_name,
    #                    repo_tag=repo_tag, options=options)

    get_image_blobs(s, container_registry=container_registry, repo_name=repo_name,
                    repo_tag=repo_tag, path=path)


def check_registry(s, container_registry):
    requests_str = container_registry
    print("Request: ", requests_str)
    print(s.get(requests_str))


def get_images(s, container_registry):

    requests_str = "{}/_catalog".format(container_registry)
    print("GET: ", requests_str)
    r = s.get(requests_str)
    print(r.json())


def get_repo_tags(s, container_registry, repo_name):
    registry_tag_request = "{}/{}/tags/list".format(
        container_registry, repo_name)
    print("Request:", registry_tag_request)
    r = s.get(registry_tag_request)
    # print(r.headers)
    print(r.json())


def get_image_manifest(s, container_registry, repo_name, repo_tag, options=None):
    registry_manifest_request = "{}/{}/manifests/{}".format(
        container_registry, repo_name, repo_tag)
    print("Request:", registry_manifest_request)
    headers = {"Accept": "application/vnd.docker.distribution.manifest.v2+json"}

    r = s.get(registry_manifest_request, headers=headers)
    if options is None:
        print(r.text)
    else:
        manifest = json.loads(r.text)
        print(json.dumps(manifest[options], indent=2))

    return json.loads(r.text)


def get_image_blobs(s, container_registry, repo_name, repo_tag, path):
    manifest = get_image_manifest(
        s=s, container_registry=container_registry, repo_name=repo_name, repo_tag=repo_tag)

    blobs = [manifest["config"]] + manifest["layers"]
    for b in blobs:
        print(b["digest"])
        digest = b["digest"]
        registry_blob_request = "{}/{}/blobs/{}".format(
            container_registry, repo_name, digest)
        print("\nRequest:", registry_blob_request)
        r = s.get(registry_blob_request, stream=True)
        with open(os.path.join(path, digest), 'wb') as fd:
            for chunk in r.iter_content(chunk_size=1024):
                fd.write(chunk)


args = ""
# parser = argparse.ArgumentParser('Send HTTP Requests to the provided Host.')
# parser.add_argument('--host', dest='host', type=str,
#                     required=True, help='URL to Host with running registry.')

# subparser = parser.add_subparsers(title='Actions')

# check = subparser.add_parser('check')

# parser.add_argument('--action', dest='action', type=str, required=False,
#                     help='Action to make for the provided URL. The default action is to check if registry exists',
#                     choices=['check', 'list_images', 'list_tags',
#                              'get_manifest', 'get_blobs'],
#                     default='check')

# args = parser.parse_args()

# print(args.host)


if __name__ == "__main__":
    # execute only if run as a script
    # main(args)
    pass
