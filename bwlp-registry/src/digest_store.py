from threading import RLock
import json


class DigestStore(object):

    def __init__(self) -> None:
        self.store = dict()
        self.lock = RLock()

    def __contains__(self, key):
        return key in self.store

    def add_entry(self, digest, name, tag):
        if digest in self.store:
            self.store[digest].add((name, tag))
        else:
            self.store[digest] = {(name, tag)}

    def get_entry(self, digest):
        return list(self.store[digest])[0]

    def insert_manifest(self, manifest: bytes, name: str, tag: str):
        manifest = json.loads(manifest)
        tmp = [manifest["config"]] + manifest["layers"]
        with self.lock:
            for l in tmp:
                self.add_entry(l["digest"], name, tag)
