from socketserver import ThreadingTCPServer
from http.server import BaseHTTPRequestHandler, HTTPServer

import sys
import logging
import os
import re
from pathlib import Path
from .dmsd_proxy import BwlpDmsdProxy
from .digest_store import DigestStore
from .registry_client import DockerHubClient
from .cache import CacheDict


logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter("%(levelname)s:%(threadName)s:%(message)s")

steamHandler = logging.StreamHandler(sys.stdout)
steamHandler.setLevel(logging.DEBUG)
steamHandler.setFormatter(formatter)

fileHandler = logging.FileHandler(__file__+".log", mode="w")
fileHandler.setLevel(logging.DEBUG)
fileHandler.setFormatter(formatter)

logger.addHandler(fileHandler)
logger.addHandler(steamHandler)


BASE_DIR = os.path.dirname(__file__)


class BwlpRegistryHandler(BaseHTTPRequestHandler):
    bwlp_container_store = BwlpDmsdProxy()
    manifest_cache = CacheDict(ttl=60*60)
    digest_store = DigestStore()

    def __init__(self, request, client_address, server) -> None:
        self.manifest_store_path = Path(
            "/srv/openslx/nfs/container_store/manifests")
        self.manifest_file = "manifest"
        self.buffer_size = 2048
        self.dh_client = DockerHubClient()
        super().__init__(request, client_address, server)

    def _set_bad_response(self, msg="File not found"):
        self.send_error(404, msg)
        self.end_headers()

    def _get_context_info(path: str, context):
        """
        /v2/<image_name>/manifests/<reference> -> extract image_name and reference
        /v2/<image_name>/blobs/<digest> -> extract image_name and digest
        """

        sep = "/"
        path_parts = path.split(sep)
        image_name = sep.join(path_parts[path_parts.index("v2") + 1:
                                         path_parts.index(context)])
        reference = path_parts[-1]
        return image_name, reference

    def _cache_manifest(path: str, header: dict, content: dict):
        logger.info(f"cache manifest: key:{path} -- header:{header}")
        val = {"header": header, "manifest": content}
        BwlpRegistryHandler.manifest_cache.add_entry(path, val)

    def _cache_get(path: str):
        entry = BwlpRegistryHandler.manifest_cache.get_entry(path)
        if entry:
            return entry

    def do_HEAD(self):
        logger.info("head request")
        if "manifests" in self.path:
            self._head_manifest()

    def _head_manifest(self):

        image_name, image_tag = BwlpRegistryHandler._get_context_info(self.path,
                                                                      "manifests")
        manifest = Path.joinpath(
            self.manifest_store_path, image_name, image_tag)
        if not manifest.is_file():
            self._head_external()
            return

        # TODO response to head_request with local manifest details
        # Content-Length: < length of manifest >
        # Docker-Content-Digest: < digest >

    def do_GET(self):
        if not self.path.startswith("/v2/"):
            self._set_bad_response()
        elif self.path == "/v2/":
            logger.info("Registry Entry Request: Path:{}".format(self.path))
            self.send_response(200),
            self.send_header("content-type", "text/json")
            self.send_header("content-length", "2")
            self.end_headers()
            self.wfile.write(b"{}")
        elif self.path == "/v2/_pullratelimit":
            self._request_ratelimit()
        elif self.path.startswith("/v2/bwlp/digest/"):
            self._request_layer()
        elif self.path.startswith("/v2/bwlp/images/"):
            self._request_images_bwlp()
        elif self.path.startswith("/v2/image/container/") and self.path.endswith("/metadata"):
            self._request_container_image_meta()
        else:
            self.registry_entry()

        self.close_connection = True

    def registry_entry(self):
        logger.debug("GET request,\nPath: %s\nHeaders:\n%s\n",
                     str(self.path), str(self.headers))
        if "manifests" in self.path:
            self._request_manifest()
        elif "blobs" in self.path:
            self._request_external()
        else:
            self._set_bad_response()

    def _request_layer(self):
        # TODO send proper Response
        digest = self.path.split("/")[-1]
        if digest in BwlpRegistryHandler.digest_store:
            (name, tag) = BwlpRegistryHandler.digest_store.get_entry(digest)
            logger.info(f"ImageName:{name} -- ImageTag:{tag}")
        self.send_response(200)

    def _request_ratelimit(self):
        dh_response = self.dh_client.request_pullratelimit()
        self.send_response(dh_response.status_code)
        self.end_headers()
        logger.info("pullratelimit: limit:{} -- remaining:{}".format(
            dh_response.headers["RateLimit-Limit"],
            dh_response.headers["RateLimit-Remaining"]))

    def _head_external(self):
        logger.info(f"delegate request to docker hub:{self.path}")
        # try to receive object from docker hub

        image_name, reference = BwlpRegistryHandler._get_context_info(self.path,
                                                                      "manifests")
        dh_response = self.dh_client.head_manifest(name=image_name,
                                                   reference=reference)

        self.send_response(dh_response.status_code)
        for k, v in dh_response.headers.items():
            self.send_header(k, v)
        self.end_headers()

    def _request_external(self):
        logger.info(f"delegate request to docker hub:{self.path}")
        # try to receive object from docker hub
        do_cache = False
        content = b""

        if "manifests" in self.path:
            image_name, reference = BwlpRegistryHandler._get_context_info(self.path,
                                                                          "manifests")
            dh_response = self.dh_client.request_manifest(name=image_name,
                                                          reference=reference)
            if dh_response.status_code == 200:
                do_cache = True
                header_tags = ["Docker-Content-Digest", "Content-Length"]
                headers = {tag: dh_response.headers[tag]
                           for tag in header_tags}

        elif "blobs" in self.path:
            image_name, digest = BwlpRegistryHandler._get_context_info(
                self.path, "blobs")
            dh_response = self.dh_client.request_blob(name=image_name,
                                                      digest=digest)
        if dh_response.status_code != 200:
            return

        self.send_response(200)
        for k, v in dh_response.headers.items():
            self.send_header(k, v)
        self.end_headers()
        for chunk in dh_response.iter_content(chunk_size=self.buffer_size):
            if do_cache:
                content += chunk
            self.wfile.write(chunk)

        if do_cache:
            BwlpRegistryHandler._cache_manifest(self.path, headers, content)
            BwlpRegistryHandler.digest_store.insert_manifest(
                content, image_name, reference)

    def _request_manifest(self):
        image_name, image_tag = BwlpRegistryHandler._get_context_info(self.path,
                                                                      "manifests")
        manifest_file = os.path.join(
            self.manifest_store_path, image_name, image_tag, self.manifest_file)

        if (cache_entry := BwlpRegistryHandler._cache_get(self.path)) is not None:
            header = cache_entry["header"]
            manifest = cache_entry["manifest"]
            logger.info(f"Use cached manifest: header{header}")

            self.send_response(200)
            self.send_header(
                "Content-type", "application/vnd.docker.distribution.manifest.v2+json")
            for k, v in header.items():
                self.send_header(k, v)
            self.end_headers()

            content_length = int(header["Content-Length"])
            bytes_send = 0
            while bytes_send < content_length:
                bytes_send += self.wfile.write(manifest[bytes_send:])

            return

        if not Path(manifest_file).is_file():
            self._request_external()
            return

        self.send_response(200)
        self.send_header(
            "Content-type", "application/vnd.docker.distribution.manifest.v2+json")
        self.send_header("Content-Length",
                         os.stat(os.path.join(BASE_DIR, manifest_file)).st_size)
        self.flush_headers()
        self.end_headers()
        with open(manifest_file, "rb") as file:
            while True:
                buffer = file.read(1024)
                if buffer:
                    self.wfile.write(buffer)
                else:
                    break

    def _request_container_image_meta(self):
        image_id = re.search("/v2/image/container/(.*)/metadata", self.path).group(1)
        response = BwlpRegistryHandler.bwlp_container_store.request_container_image_meta(image_id)
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", f"{len(response)}")
        self.end_headers()
        self.wfile.write(response)

    def _request_images_bwlp(self):
        """
        Uses the BwlpContainerStore to retrive a list of
        defined Images in bwlp.
        """
        response = BwlpRegistryHandler.bwlp_container_store.request_cluster_container()
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.send_header("Content-Length", f"{len(response)}")
        self.end_headers()
        self.wfile.write(response)


def run_server(server_class=HTTPServer, handler_class=BwlpRegistryHandler, port=9081):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    logger.info('Starting httpd...\n')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    logger.info('Stopping httpd...\n')


if __name__ == '__main__':
    run_server()
