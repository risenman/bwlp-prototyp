
from time import time
from threading import RLock


class CacheEntry():
    def __init__(self, value, ttl):
        self.string = value
        self.expires_at = time() + ttl
        self._expired = False

    def value(self):
        return self.string

    def expired(self):
        if self._expired is False:
            return (self.expires_at < time())
        else:
            return self._expired


class CacheDict():
    def __init__(self, ttl=20):
        self.entries = {}
        self.ttl = ttl
        self.lock = RLock()

    def add_entry(self, key, value):
        with self.lock:
            self.entries[key] = CacheEntry(value, self.ttl)

    def get_entry(self, key) -> str:
        with self.lock:
            if key in self.entries.keys():
                if not self.entries[key].expired():
                    return self.entries[key].value()
                else:
                    self.entries.pop(key)
