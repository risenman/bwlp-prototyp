import requests
# TODO logging, configuration

class BwlpDmsdProxy(object):
    # uses database connection to bwlp to get information about defind containers
    def __init__(self):
        self.bwlp_dmsd = "127.0.0.1"  # dmsd websever is only at localhost available
        self.bwlp_dmsd_port = 9080

    def request_cluster_container(self):
        url = f"http://{self.bwlp_dmsd}:{self.bwlp_dmsd_port}/bwlp/container/clusterimages"
        r = requests.get(url=url)
        assert 200 <= r.status_code <= 299
        return r.content

    def request_container_image_meta(self, image_id):
        url = f"http://{self.bwlp_dmsd}:{self.bwlp_dmsd_port}/image/container/{image_id}/metadata"
        r = requests.get(url=url)
        assert 200 <= r.status_code <= 299
        return r.content
