#!/bin/bash

# do we realy need to check the user?
# if [[ $(whoami) != "demo" ]]
# then
#     echo "Not expected user"
#     exit -1 
# fi

if [[ -z "$TAR_FILE" ]]
then
    echo "TAR_FILE not set"
    exit -1
fi

if [[ -z "$BUILD_DIR" ]]
then
    BUILD_DIR="/home/demo/build_dir"
fi

# check for running container and remove them
if [[ -n $(docker ps -a -q) ]]
then
    docker rm -f $(docker ps -a -q)
fi

TAR_FILE="$BUILD_DIR/$TAR_FILE"

if [[ ! -e "$TAR_FILE" ]]
then
    echo "TAR_FILE not exists $TAR_FILE"
    exit -1
fi

CONTAINER_META="container_meta.json"
PREBUILD_MANIFEST="manifest.json"

# check TAR_FILE to select the proper BUILD_METHOD
# 
if [[ -n "$(tar -tf $TAR_FILE | grep $CONTAINER_META)" ]]
then
    BUILD_METHOD="BUILD"
elif [[ -n "$(tar -tf $TAR_FILE | grep $PREBUILD_MANIFEST)" ]]
then
    BUILD_METHOD="PREBUILD"
else
    echo "FAILED: COULD NOT DETERMINE BUILD_METHOD"
    exit -1
fi


function build_image()
{
    echo "# build image"
    tar -xf "$TAR_FILE" --directory=$BUILD_DIR

    BUILD_CONTEXT=$(jq -r .build_context_url $BUILD_DIR/$CONTAINER_META)
    if [[ dockerfile == "$BUILD_CONTEXT" ]]
    then
        BUILD_CONTEXT=$BUILD_DIR
    fi

    REPO_TAG="tmp:latest"
    # TODO collect build output
    docker build -t $REPO_TAG $BUILD_CONTEXT
    [ "$?" != 0 ] && echo "FAILED: docker build -t REPO_TAG:$REPO_TAG BUILD_CONTEXT:$BUILD_CONTEXT" && exit -1
}


function prebuild_image()
{
    echo "# inspect and load TAR_FILE $TAR_FILE"
    # TODO currently only one container image expected in tar file
    REPO_TAG=$(tar -axf $TAR_FILE manifest.json -O | jq -r '.[].RepoTags[0]')
    docker load --input $TAR_FILE
    [ "$?" != 0 ] && echo "FAILED: docker load --input TAR_FILE:$TAR_FILE" && exit -1
}

if [[ "$BUILD_METHOD" == "BUILD" ]]
then
    build_image
elif [[ "$BUILD_METHOD" == "PREBUILD" ]]
then
    prebuild_image
else
    echo "FAIL: WRONG BUILD_METHOD"
    exit -1
fi

echo "# start local container registry"
# run a local container registry and let some time pass by 
docker run -d --rm --name tmp_registry -p 0.0.0.0:5000:5000 registry:2 > /dev/null 
[ "$?" != 0 ] && echo "FAILED: start local registry" && exit -1
sleep 1

echo "# tag image and push to local registry"
TMP_FILE=$(mktemp)
docker tag $REPO_TAG "localhost:5000/$REPO_TAG"
[ "$?" != 0 ] && echo "FAILED: docker tag $REPO_TAG localhost:5000/$REPO_TAG" && exit -1
docker push "localhost:5000/$REPO_TAG" > "$TMP_FILE"
[ "$?" != 0 ] && echo "FAILED: docker push localhost:5000/$REPO_TAG" && exit -1

# simple check if push was successful
# last line should contain a manifests digest
if [[ -z $(cat "$TMP_FILE" | grep "digest: sha") ]]
then
    echo "push not successful!"
    exit -1
fi

MANIFEST_DIR="$BUILD_DIR/manifest"
LAYER_DIR="$BUILD_DIR/layer"
[ -e "$MANIFEST_DIR" ] && rm -r -- "$MANIFEST_DIR"
[ -e "$LAYER_DIR" ] && rm -r -- "$LAYER_DIR"

mkdir "$MANIFEST_DIR"
mkdir "$LAYER_DIR"

IMAGE_NAME=$(echo $REPO_TAG | cut -d ":" -f 1)
MANIFEST_DIGEST=$(cat $TMP_FILE | tail -n 1 | cut -d " " -f 3)
echo "# download manifest and layers from registry: image:$IMAGE_NAME"
echo "manifest digest: $MANIFEST_DIGEST"
curl -s "localhost:5000/v2/$IMAGE_NAME/manifests/$MANIFEST_DIGEST" --output "$MANIFEST_DIR/$MANIFEST_DIGEST"
LAYER_DIGESTS=$(cat "$MANIFEST_DIR/$MANIFEST_DIGEST" | grep "digest" | xargs -L 1 | cut -d " " -f 2)
for digest in $LAYER_DIGESTS
do
    echo "layer digest: $digest"
    curl -s "localhost:5000/v2/$IMAGE_NAME/blobs/$digest" --output "$LAYER_DIR/$digest"
done

echo "# SUCCESS"
