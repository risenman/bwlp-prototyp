from paramiko import SSHClient, client
from threading import Thread
from pathlib import Path
from queue import Queue

from paramiko.sftp_client import SFTPClient

PROJECT_PATH = Path(__file__).resolve().parent.parent


class BwlpImageBuildHandle(object):

    def __init__(self):
        self.ssh_username = "demo"
        self.ssh_userhome = Path("/home/demo")
        self.build_script = PROJECT_PATH.joinpath("src/remote_build.sh")
        self.ssh_host = "141.79.65.238"
        self.ssh_key_file = Path.joinpath(PROJECT_PATH, ".ssh/bwlp_client_rsa")
        self.ssh_client = None
        self.job_queue = Queue()
        self.job_thread = Thread(target=self._do_job, daemon=True).start()

        self.remote_build_dir = self.ssh_userhome.joinpath("build_dir")

        # TODO should be done and managed by config
        self.container_store = Path("/tmp/container_store/").absolute()
        self.container_store.mkdir(mode=0o777, parents=True, exist_ok=True)
        self.blob_path = self.container_store.joinpath("blobs")
        self.blob_path.mkdir(mode=0o777, parents=True, exist_ok=True)
        self.manifest_path = self.container_store.joinpath("manifests")
        self.manifest_path.mkdir(mode=0o777, parents=True, exist_ok=True)

    def _is_connected(self):
        if self.ssh_client is None:
            return False
        elif self.ssh_client.get_transport() is None:
            return False
        elif self.ssh_client.get_transport().is_active():
            return True
        else:
            return False

    def _open_connection(self):

        assert self.ssh_key_file.is_file(), "key file not exist"

        self.ssh_client = SSHClient()
        self.ssh_client.set_missing_host_key_policy(
            policy=client.WarningPolicy())
        self.ssh_client.connect(hostname=self.ssh_host,
                                username=self.ssh_username,
                                key_filename=self.ssh_key_file.as_posix(),
                                )

    def _close_connection(self):
        if self.ssh_client:
            self.ssh_client.close()

    def _exec_remote_cleanup(self):

        remote_build_script = self.ssh_userhome.joinpath(
            self.build_script.name)

        command = (
            f"[ -e '{remote_build_script}' ] && rm -- '{remote_build_script}';"
            f"[ -e '{self.remote_build_dir}' ] && rm -r -- '{self.remote_build_dir}'"
        )

        self.ssh_client.exec_command(command=command)

    def _handle_job(self, job_description: dict):

        image_name = job_description["name"]
        image_tag = job_description["tag"]
        image_tar_file = Path(job_description["image_tar_file"]).resolve()

        print("open connection")
        self._open_connection()
        self._exec_remote_cleanup()
        print("put resources to remote")
        self._put_to_remote(image_tar_file)
        print("execute remote build")
        digests = self._exec_remote_build(image_tar_file.name)
        print("get resources form remote")
        self._get_from_remote(image_name, image_tag, digests)
        self._exec_remote_cleanup()

    def _put_to_remote(self, image_tar_file: Path):

        sftp_client = self.ssh_client.open_sftp()
        sftp_client.put(str(self.build_script),
                        str(self.ssh_userhome.joinpath(self.build_script.name)))
        sftp_client.mkdir(str(self.remote_build_dir))
        sftp_client.put(str(image_tar_file.absolute()),
                        str(self.remote_build_dir.joinpath(image_tar_file.name)))

    def _get_from_remote(self, image_name, image_tag, digests: dict):

        image_manifest_path = self.manifest_path.joinpath(
            image_name, image_tag)

        if(image_manifest_path.exists()):
            # TODO how do we handle multiple manifests? mapping imageID -> Digest
            pass
        
        image_manifest_path.mkdir(mode=0o777, parents=True, exist_ok=True)
        image_manifest_file = image_manifest_path.joinpath(digests["manifest"])

        sftpc = self.ssh_client.open_sftp()
        # check if manifest already exists
        self._sftp_get(sftpc,
                       self.remote_build_dir.joinpath("manifest",
                                                      digests["manifest"]),
                       image_manifest_file)

        for digest in digests["layer"]:
            layer_file = self.blob_path.joinpath(digest)
            self._sftp_get(sftpc,
                           self.remote_build_dir.joinpath("layer",
                                                          digest),
                           layer_file)
        sftpc.close()

    def _sftp_get(self, sftpc: SFTPClient, remotepath: Path, localfile: Path):
        if localfile.exists():
            # do not copy existing layers
            print(
                f"localfile {localfile} already exists, do not copy")
        else:
            sftpc.get(remotepath=str(remotepath),
                      localpath=str(localfile))

    def _exec_remote_build(self, tar_filename):
        command = (
            f"export TAR_FILE='{tar_filename}'; "
            "bash remote_build.sh ")
        (_, stdout, stderr) = self.ssh_client.exec_command(command=command)

        digests = {}
        digests["layer"] = []

        while True:
            remote_out = str(stdout.readline())
            remote_out = remote_out.strip()
            # TODO log remote output
            print("remote_exec: ", remote_out)
            if remote_out.startswith("manifest digest:"):
                digests["manifest"] = remote_out.split(" ")[-1]
            elif remote_out.startswith("layer digest:"):
                digests["layer"].append(remote_out.split(" ")[-1])
            elif "SUCCESS" in remote_out:
                break
            elif not remote_out:  # empty sting
                remote_err = stderr.read()
                # TODO log remote output
                print("remote_exec failed:\n\tSTDOUT:",
                      remote_out, "\n\tSTDERR:", remote_err)
                digests = None
                break

        return digests

    def _do_job(self):
        while True:
            job = self.job_queue.get()
            print(f"do job {job}")

            # TODO add handling for failed jobs
            # retry_counter in job
            try:
                self._handle_job(job)
            except:
                print(f"JOB FAILED: {job}")

    def add_job(self, id, job_description):
        self.job_queue.put(item=job_description)
