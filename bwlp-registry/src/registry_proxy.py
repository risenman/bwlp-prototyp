
import socket
import sys
import datetime
import threading
import os
import logging
import subprocess
import signal

import json
from time import sleep

# This script is a poc for a proxy to relay pull request of a docker client to any desired docker registry.
# Currently this works only with insecure http registries and only pull request are tested.

# to start the proxy:
# change SERVER_ADDR to the desired hostname and run script with
# python3 registry-proxy.py 20000

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter("%(levelname)s:%(threadName)s:%(message)s")

steamHandler = logging.StreamHandler(sys.stdout)
steamHandler.setLevel(logging.DEBUG)
steamHandler.setFormatter(formatter)

fileHandler = logging.FileHandler("registry_proxy.log", mode="w")
fileHandler.setLevel(logging.DEBUG)
fileHandler.setFormatter(formatter)

logger.addHandler(fileHandler)
logger.addHandler(steamHandler)


# global variables
max_connections = 20
BUFFER_SIZE = 1024
SERVER_ADDR = "bwgpul2.cit.hs-offenburg.de"
SERVER_PORT = 5000

BWLP_SAT_ADDR = "localhost"
BWLP_SAT_PORT = 8080


BASE_DIR = os.path.dirname(__file__)

hostname = socket.gethostname()
ip_address = socket.gethostbyname(hostname)


def handle_request(client):
    try:
        logger.info("Receive Request")

        data = client.recv(BUFFER_SIZE)
        head = parse_head(data)

        headers = head["headers"]
        headers["host"] = str(ip_address)

        request = encode_response(head)

        (http_method, url_path, http_addendum) = head["meta"].split(
            ' ')  # split on whitespace,

        if http_method == "GET":
            do_GET(head, url_path, request, client)
        if http_method == "HEAD":
            do_HEAD(client, request)

    except UnicodeDecodeError:
        print("UnicodeDecodeError")
    finally:
        client.close()


def do_HEAD(client, request):
    send_to_server(client=client,
                   host=BWLP_SAT_ADDR,
                   port=BWLP_SAT_PORT,
                   data=request,
                   is_head=True)


def do_GET(head, url_path, request, client):
    # split url_path by / and remove empty strings
    url_parts = [a for a in url_path.split("/") if '' != a]

    if len(url_parts) == 0 or url_parts[0] != "v2":
        pass

    else:
        if "bwlp" in url_parts:
            sep = "/"
            if "manifests" in url_parts:
                logger.info(
                    "bwlp: handle manifest request request")
                image_name = sep.join(url_parts[url_parts.index("bwlp") + 1:
                                                url_parts.index("manifests")])
                file_path = os.path.join(BASE_DIR, "manifest.json")

                local_get_file(client,
                               type="manifest",
                               file_path=file_path,
                               image_name=image_name)

            elif "blobs" in url_parts:
                logger.info(
                    "bwlp: handle blob request request")
                image_name = sep.join(url_parts[url_parts.index("bwlp") + 1:
                                                url_parts.index("blobs")])
                digest = url_parts[-1]
                file_path = os.path.join(BASE_DIR, digest)
                local_get_file(client,
                               type="blob",
                               file_path=file_path,
                               image_name=image_name,
                               digest=digest)

        elif "dnbd3" in url_parts:
            sep = "/"
            if "dnbd3/" in url_path:
                head["meta"] = head["meta"].replace("dnbd3/", "")
            elif "dnbd3" in url_path:
                head["meta"] = head["meta"].replace("dnbd3", "")
            head["headers"]["host"] = str(ip_address)
            server_request = encode_response(head)
            logger.debug(
                f"dnbd3: handle request: path: {head['meta']}")
            if len(url_parts) == 2:

                send_to_server(client=client,
                               host="141.79.65.237",
                               port=8080,
                               data=server_request)

            elif "manifests" in url_parts:
                bwlp_manifest_request(client=client,
                                      host="141.79.65.237",
                                      port=8080,
                                      data=server_request)
                return

            elif "blobs" in url_parts:
                image_name = sep.join(url_parts[url_parts.index("dnbd3") + 1:
                                                url_parts.index("blobs")])
                digest = url_parts[-1]

                dnbd3_get_file(client,
                               type="blob",
                               digest=digest)

        else:
            send_to_server(client, BWLP_SAT_ADDR, BWLP_SAT_PORT, request)


def ok_response():
    response = {
        'meta': 'HTTP/1.1 200 OK',
        'headers': {
            'content-length': '2',
            'content-type': 'application/json; charset=utf-8',
            'docker-distribution-api-version': 'registry/2.0',
            'x-content-type-options': 'nosniff',
            'date': '',
            'connection': 'close'}
    }

    response["headers"]["date"] = datetime.datetime.now().strftime(
        "%a, %d %b %Y %X %Z")

    return encode_response(response)


def encode_response(response):

    logger.debug("Encode Response: {}".format(response))
    retval = "{}\r\n".format(response["meta"])
    for key, value in response["headers"].items():
        retval += "{}: {}\r\n".format(key, value)
    retval += "\r\n"
    return retval.encode()


def exec_subprcess(cmd) -> subprocess.Popen:

    p = subprocess.Popen(cmd,
                         stdout=subprocess.PIPE,
                         encoding="UTF8")
    logger.info("PID of Subprocess: {}".format(p.pid))
    return p


dnbd3_cmd = "dnbd3-fuse"
dnbd3_opt = " -f -o allow_other,max_readahead=262144"
dnbd3_opt_default_host = " -h 141.79.65.237"
dnbd3_opt_manifest_path = " -i container_store/manifests/{}/manifest"
dnbd3_opt_blob_path = " -i container_store/blobs/{}"

dnbd3_file = "img"
dnbd3_parent_dir = "/tmp/docker/dnbd3"


def dnbd3_mount_file(dnbd3_opt_remote_path, dnbd3_opt_local_mount, execute=exec_subprcess):

    logger.info("Create local mount point: {}".format(dnbd3_opt_local_mount))
    create_mount_dir(dnbd3_opt_local_mount)

    cmd = dnbd3_cmd + \
        dnbd3_opt + \
        dnbd3_opt_default_host + \
        dnbd3_opt_remote_path + " " + \
        dnbd3_opt_local_mount

    cmd = cmd.split()

    logger.info("Execute dnbd3 Command as Subprocess -- CMD: {}".format(cmd))
    p = execute(cmd)
    # wait for dnbd3-fuse until ImagePathName is ready
    while True:
        dnbd3_fuse_output = p.stdout.readline()
        logger.debug("dnbd3_fuse_output: {}".format(dnbd3_fuse_output))
        if "ImagePathName" in dnbd3_fuse_output:
            logger.info("Subprocess dnbd3-fuse successfully started.")
            break
        elif dnbd3_fuse_output == "":
            p.send_signal(signal.SIGINT)
            raise ChildProcessError()
        else:
            continue
    return p


def dnbd3_get_file(client, type, image_name=None, image_tag=None, digest=None):
    local_mount = ""
    remote_path = ""
    file_length = None
    if type == "manifest":
        local_mount = os.path.join(dnbd3_parent_dir, "manifest")
        remote_path = dnbd3_opt_manifest_path.format(
            image_name + "/" + image_tag)
    elif type == "blob":
        local_mount = os.path.join(dnbd3_parent_dir, digest)
        remote_path = dnbd3_opt_blob_path.format(digest)
        file_length = layer_info_cache[digest]

    logger.info("Mount requested file via dnbd3")
    try:
        p = dnbd3_mount_file(dnbd3_opt_remote_path=remote_path,
                             dnbd3_opt_local_mount=local_mount)
    except ChildProcessError:
        logger.error("Subprocess dnbd3-fuse not started")
        return -1

    try:
        response = prepare_http_header(type,
                                       content_length=file_length,
                                       docker_content_digest=digest)

        file_name = os.path.join(local_mount, dnbd3_file)
        client.sendall(encode_response(response))

        with open(file_name, mode="rb") as file_obj:
            # we read here a fuse-mounted file via dnbd3.
            # because of dnbd3 an its padding the file is lager than the real.
            # so we need be to be sure only file_length size of file is transmitted.
            client.sendfile(file_obj, count=file_length)

    # TODO React on Exception (PipeBrokeError)
    #

    finally:
        p.send_signal(sig=signal.SIGINT)
        client.close()


base_response = {
    'meta': 'HTTP/1.1 200 OK',
    'headers': {
        'docker-distribution-api-version': 'registry/2.0',
        'x-content-type-options': 'nosniff',
        'connection': 'close'
    }
}

manifest_headers = {
    'content-type': 'application/vnd.docker.distribution.manifest.v2+json',
    'docker-content-digest': 'sha256:9638b269895cb2c8db1bcbefcb1aa6eb8f2dbf98cbaefe94307448e45ce78108',
    'etag': '"sha256:9638b269895cb2c8db1bcbefcb1aa6eb8f2dbf98cbaefe94307448e45ce78108"'
}

blob_header = {
    'accept-ranges': 'bytes',
    'cache-control': 'max-age=31536000',
    'content-type': 'application/octet-stream'
}


def prepare_http_header(type_, content_length, docker_content_digest=None):

    response = base_response
    if type_ == "manifest":
        response["headers"] = {**response["headers"], **manifest_headers}

    elif type_ == "blob":
        response["headers"] = {**response["headers"], **blob_header}
        response["headers"]["docker-content-digest"] = docker_content_digest
        response["headers"]["etag"] = docker_content_digest
    else:
        raise TypeError

    response["headers"]["content-length"] = content_length
    response["headers"]["date"] = datetime.datetime.now().strftime(
        "%a, %d %b %Y %X %Z")

    logger.info("http response: {}".format(response))
    return response


def create_mount_dir(parent: str):
    if not os.path.isdir(parent):
        os.makedirs(parent, exist_ok=True)
    else:
        # what to do ? maybe remove?
        None


def read_in_chunks(file_obj, chunk_size=BUFFER_SIZE):
    while True:
        data = file_obj.read(chunk_size)
        if not data:
            break
        yield data


def local_get_file(client: socket.socket, type, file_path, image_name, digest=None):

    if not os.path.isfile(file_path):
        raise FileNotFoundError("File: {} not exists".format(file_path))

    logger.info("Respond with local file: Image: {} -- File: {}".format(
        image_name, file_path))
    response = prepare_http_header(
        type, content_length=os.stat(file_path).st_size, docker_content_digest=digest)

    with open(file_path, 'rb') as file_obj:
        for data in read_in_chunks(file_obj):
            if response:
                # send only once with response header
                data = encode_response(response) + data
                response = None

            client.sendall(data)
    client.close()


layer_info_cache = dict()


def send_to_server(client, host, port, data, is_head=False):

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.connect((socket.gethostbyname(host), port))
    server.sendall(data)

    server_response = server.recv(BUFFER_SIZE)
    head = parse_head(server_response)

    client.sendall(server_response)

    if is_head:
        server.close()
        return

    content_length = int(head["headers"]["content-length"])
    current_length = len(head["chunk"])

    while current_length < content_length:
        server_response = server.recv(BUFFER_SIZE)
        current_length += len(server_response)
        client.sendall(server_response)

    server.close()
    client.close()


def bwlp_manifest_request(client, host, port, data,):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.connect((socket.gethostbyname(host), port))
    server.sendall(data)

    server_response = server.recv(BUFFER_SIZE)
    head = parse_head(server_response)

    client.sendall(server_response)

    content_length = int(head["headers"]["content-length"])
    current_length = len(head["chunk"])

    tmp_server_response = server_response

    while current_length < content_length:
        server_response = server.recv(BUFFER_SIZE)
        tmp_server_response += server_response
        current_length += len(server_response)
        client.sendall(server_response)

    # get the content part json manifest
    tmp_server_response = tmp_server_response.split(b"\r\n\r\n")[
        1].decode("utf-8")
    tmp_server_response = json.loads(tmp_server_response)

    layer_infos = [tmp_server_response["config"]] + \
        tmp_server_response["layers"]
    for i in layer_infos:
        layer_info_cache[i["digest"]] = i["size"]

    logger.debug("Manifest inspected: {}".format(layer_info_cache))

    server.close()
    client.close()


def parse_head(head_request):

    nodes = head_request.split(b"\r\n\r\n")
    heads = nodes[0].split(b"\r\n")
    meta = heads.pop(0).decode("utf-8")
    data = {
        "meta": meta,
        "headers": {},
        "chunk": b""
    }

    if len(nodes) >= 2:
        data["chunk"] = nodes[1]

    for head in heads:
        pieces = head.split(b": ")
        key = pieces.pop(0).decode("utf-8")
        if key.startswith("Connection: "):
            data["headers"][key.lower()] = "close"
        else:
            data["headers"][key.lower()] = b": ".join(pieces).decode("utf-8")

    logger.debug("parse_header:\n{}".format(data))
    return data


# This function initializes socket and starts listening.
# When connection request is made, a new thread is created to serve the request
def start_proxy_server(proxy_port):

    # Initialize socket
    try:
        proxy_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        proxy_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        proxy_socket.bind(('', proxy_port))
        proxy_socket.listen(max_connections)

        logger.info("Serving Proxy on {} port {} ..."
                    .format(str(proxy_socket.getsockname()[0]),
                            str(proxy_socket.getsockname()[1])
                            ))
    except Exception as e:
        logger.error("Error starting proxy server ...\n{}".format(e))
        proxy_socket.close()
        raise SystemExit

    # Main server loop
    while True:
        client_socket = None
        try:
            (client_socket, client_addr) = proxy_socket.accept()

            d = threading.Thread(
                target=handle_request,
                args=(client_socket,))
            d.setDaemon(True)
            d.start()

        except KeyboardInterrupt:
            if client_socket:
                client_socket.close()
            proxy_socket.close()
            print("\nProxy server shutting down ...")
            break


def main():
    proxy_port = None
    # take command line argument
    if len(sys.argv) != 2:
        print("Usage: python %s <PROXY_PORT>" % sys.argv[0])
        print("Example: python %s 20000" % sys.argv[0])
        raise SystemExit
    try:
        proxy_port = int(sys.argv[1])
    except:
        print("provide proper port number")
        raise SystemExit

    logger.info("Hostname: {} IP Address: {}".format(hostname, ip_address))
    start_proxy_server(proxy_port)


if __name__ == '__main__':
    main()
